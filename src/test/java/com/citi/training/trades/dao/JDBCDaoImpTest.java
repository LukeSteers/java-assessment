package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.model.Trade;
@Component
public class JDBCDaoImpTest {

	@Autowired
	JDBCDaoImp tradeDaoImp;
	@Test
	public void testCreateThenFindById() {
		String tstock = "AAA";
		double tprice = 1.01;
		int tvolume = 2;
		tradeDaoImp.create(new Trade(tstock,tprice,tvolume));
		assertEquals(tradeDaoImp.findById(1).getStock(),tstock);
		assertEquals(tradeDaoImp.findById(1).getVolume(),tvolume);
		assert(tradeDaoImp.findById(1).getPrice()==tprice);
	}

	//@Test
	//public void testCreatethenDeleteById() {
	//	fail("Not yet implemented");
	//}

}
