package com.citi.training.trades.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.citi.training.trades.model.Trade;

public class IntegrationTest {
	
	@Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getEmployee_returnsEmployee() {
        restTemplate.postForEntity("/trades",
                                   new Trade("BBB",3.23,14), Trade.class);

        ResponseEntity<List<Trade>> getAllResponse = restTemplate.exchange(
                                "/trades",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("Joe"));
        assert(getAllResponse.getBody().get(0).getPrice()==3.23);
        assert(getAllResponse.getBody().get(0).getVolume()==14);

    }

}
