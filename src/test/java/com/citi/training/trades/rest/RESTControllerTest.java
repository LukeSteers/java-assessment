package com.citi.training.trades.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(RESTController.class)
public class RESTControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TradeService tradeService;

    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade("AA",10,15);
        this.mockMvc
                .perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated()).andReturn();
}}
