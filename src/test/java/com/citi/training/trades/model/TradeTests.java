package com.citi.training.trades.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TradeTests {

	@Test
	public void testConstructor() {
		String tstock = "AAA";
		double tprice = 1.01;
		int tvolume = 2;
	    Trade testTrade= new Trade(tstock, tprice, tvolume);
	    //assertEquals(testTrade.getId(),1);
	    assertEquals(testTrade.getStock(),tstock);
	    assert(testTrade.getPrice()==tprice);
	    assertEquals(testTrade.getVolume(),tvolume);
	}
	
	@Test
	public void testToString() {
		String tstock = "AAA";
		double tprice = 1.01;
		int tvolume = 2;
		Trade testTrade= new Trade(tstock, tprice, tvolume);
		assertEquals(testTrade.toString(),"Trade [id=" + testTrade.getId() + ", stock=" + tstock + ", price=" + tprice + ", volume=" + tvolume + "]");
	}
}
	    
	
