package com.citi.training.trades.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.JDBCDao;
import com.citi.training.trades.model.Trade;

@Component
public class TradeService {
	@Autowired
	private JDBCDao tradeDao;

    public Trade findById(int id) {
    	return tradeDao.findById(id);
    }
    public Trade create(Trade trade) {
    	return tradeDao.create(trade);
    }
    public void deleteById(int id) {
    	tradeDao.deleteById(id);
    }
}
