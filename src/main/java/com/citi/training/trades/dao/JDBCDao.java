package com.citi.training.trades.dao;

import com.citi.training.trades.model.Trade;

public interface JDBCDao {

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);
}

