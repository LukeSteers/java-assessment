package com.citi.training.trades.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Trade {
	
    private static AtomicInteger idGenerator = new AtomicInteger(0);
	private int id;
	private String stock;
	private double price;
	private int volume;	
	
    public Trade() {
        this.id = idGenerator.getAndAdd(1);
        this.stock = "NULL";
        this.price = 0;
        this.volume = 0;
    }
	
    public Trade( String stock, double price, int volume) {
        this.id = idGenerator.getAndAdd(1);
        this.stock = stock;
        this.price = price;
        this.volume = volume;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", price=" + price + ", volume=" + volume + "]";
	}
    
}
